# py 3720 64
# test_multiply_long.py
from core.multiply import multiply_long
import pytest

@pytest.mark.parametrize("num1, num2", [(5,5), (10, 9), (87, 20), (32, 0), (42, 1)])
def test_multiply_long(num1, num2):
    assert multiply_long(num1, num2) == (num1 * num2)
    #assert multiply_long(1.9, 90) should fail!
